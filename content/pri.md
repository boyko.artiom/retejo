+++
headless = true
+++

## Pri nia servilo
EsperoNova estas la plej nova Minecraft-komunumo dediĉita sole al la uzo de Esperanto, la plej populara artefarita
lingvo en la mondo. Kaj komencantoj kaj fluaj parolantoj estas bonvenaj ĉi tie.
La servilo estis kreita tuj post kiam alia Esperanta Minecraft-servilo, kies nomo estis "EsperaMondo", estis fermita kaj malŝaltita. Ĝi estis aktiva dum ses jaroj kaj estis la plej granda Minecraft-servilo por esperantistoj. 
Ni kreis ĉi tiun servilon por revivigi la projekton. Se vi ŝatus kontribui al nia projekto, ni invitas vin aliĝi al nia Discord-servilo.
Bonvolu tralegi la servilajn regulojn kaj aliajn informojn antaŭ ol ekludi.
