+++
title = "Bonvenon"
linktitle = "Ĉefpaĝo"
simbolo = "ĉefpaĝo"
ligiloj = [
    { href = "elŝuti", titlo = "Elŝuti la mondmapon", simbolo = "elŝuti" },
    { href = "ekludi", titlo = "Kiel ekludi",         simbolo = "ekludi" },
    { href = "mapo",   titlo = "Enreta mapo",         simbolo = "mapo" }
]
[menu.cxefa]
    weight = 1
+++

<em-karuselo class="image is-16x9">![EsperoNova emblemo](ĉefpaĝo/1.svg)
![Nova Minecraft-servilo nur por esperantistoj](ĉefpaĝo/2.webp)
![Servilo-adreso: esperonova.net](ĉefpaĝo/3.webp)
![RIP EsperaMondo 2015−2022](ĉefpaĝo/4.webp)
</em-karuselo>

# Bonvenon al EsperoNova!

Jen la oficiala retpaĝo por EsperoNova - nova kaj freŝa Minecraft-servilo nur por esperantistoj.
Ĉi tie troviĝas ĉiuj informoj necesaj por ekludi, informiĝi, kaj amuziĝi en la servilo.
Ni esperas ke vi provos la servilon, kaj ke vi trovos ĝin afabla kaj amuza loko por uzi kaj lerni Esperanton.
