+++
title = "EsperoNova lanĉiĝis!"
date = 2022-12-10
slug = "lancxo"
auxtoro = "Jonizulo"
+++

![Lancxo](lancxo.png)

Ni feliĉas anonci, ke la nova Minecraft-servilo por esperantistoj "EsperoNova" lanĉiĝis. Tuj post kiam estis anoncita, ke EsperaMondo estis fermita, entuziasmaj membroj ekkomencis novan projekton por krei Minecraft-servilon. Ni kune faris multajn decidojn, dizajnis retejon kaj instalis novan servilon kaj nun la servilo estas en tiom bona stato, ke ni lanĉis ĝin. Ni antaŭĝojas vidi novajn belajn kontruaĵojn kaj urbojn kaj ni esperas, ke tiu ĉi servilo restos aktiva dum longa tempo.

Por plifaciligi ludadon en la servilo, ni enkondukis utilajn komandojn, kiuj povas ekz. venigi kaj aliri al ludantoj aŭ teleporti vin al via lito. Alia nova ŝanĝo estas, ek nun uzantoj de senpagaj klientoj ankaŭ povas eniri la servilon.

Se vi volas helpi nin prilabori la projekton, nepre aliĝu al la Discord-servilo por kune decidi kaj paroli kun aliaj ludantoj. Eblas, ke ni kreos pliajn kontojn por social retoj poste.

Ni esperas, ke vi ĝuos la novan servilon!

- La teamo de EsperoNova (Jonizulo, ViglaPorko, vpzoom)
