+++
title = "Elŝuti la mondmapon"
simbolo = "elŝuti"
+++

# Elŝuti EsperaMondon por unu-ludanta reĝimo

<div class="info">

## Antaŭ ol vi elŝutos la mondmapon

Antaŭ ol vi elŝutos la mondmapon, certiĝu ke via komputilo havas sufiĉan spacon por ĝi. La densigita dosiero havas proksimume 7.5GB, kiu fariĝas pli ol 12GB post maldensigo; tial, estas rekomendite ke via komputilo havu **almenaŭ 25GB** da libera spaco por konservi ĉiujn dosierojn.

Ankaŭ, pro ke la dosiero estas grandega, la elŝuto bezonos multe da tempo: ~20 minutojn per rapida konekto aŭ plurajn horojn per malrapida. Bonvolu havi paciencon.

</div>

## Kiel elŝuti kaj uzi la mondmapon

1.  **Elŝutu la mondmapon per la ligiloj sube.**
2.  **Movu la mondmapon en vian Minecraft-dosierujon, pli precize en la dosierujon “.minecraft/saves/”.**
    (serĉu enrete “how to find minecraft folder” 🐊)
3.  **Certiĝu ke vi uzas la ĝustan Minecraft version.**
    (1.12.2 por EsperaMondo I, 1.15.2 por EsperaMondo II)
4.  **Ekludu en unu-ludanta reĝimo. Se EsperaMondo montriĝas, la elŝuto estis sukcesa!**

_Pli detalaj klarigoj aperos ĉi tie baldaŭ… kontrolu pli poste._

---

## Ligiloj

- EsperaMondo I  
  (malnova mondo, 1.12.2)
  [![EsperaMondo I](esperamondoI.png)](/EsperaMondo.zip)

- EsperaMondo II  
  (nuna mondo, 1.15.2)
  [![EsperaMondo II](esperamondoII.png)](/EsperaMondoII.zip)
